package ru.tinkoff.board.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

@Entity(name = "Board")
@SuperBuilder
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="fintech_boards")
@NamedEntityGraph(
        name = "board-entity-graph",
        attributeNodes = {
                @NamedAttributeNode(value = "tasks", subgraph = "tasks-subgraph")
        }, subgraphs = {
                @NamedSubgraph(name = "tasks-subgraph",
                attributeNodes = {
                        @NamedAttributeNode(value = "user"),
                        @NamedAttributeNode(value = "board"),
                        @NamedAttributeNode(value = "status")
                })
})
public class Board extends BaseForm{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "board_sequence_gen")
    @SequenceGenerator(name = "board_sequence_gen", sequenceName = "board_sequence_gen", allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "description", length = 1023)
    private String description;

    @OneToMany(mappedBy = "board", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, orphanRemoval = true)
    @ToString.Exclude
    private List<Task> tasks;

    @OneToMany(mappedBy = "board", cascade = CascadeType.PERSIST, orphanRemoval = true)
    @ToString.Exclude
    private List<Status> statuses;

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {return false;}
        Board other = (Board) o;
        return id != null & id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
