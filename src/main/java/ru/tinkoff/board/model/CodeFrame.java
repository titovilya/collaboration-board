package ru.tinkoff.board.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "CodeFrame")
@SuperBuilder
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="fintech_code_frames")
@NamedEntityGraph(
        name = "code-frame-entity-graph",
        attributeNodes = {@NamedAttributeNode(value = "user")}
)
public class CodeFrame extends BaseForm{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "code_frame_sequence_gen")
    @SequenceGenerator(name = "code_frame_sequence_gen", sequenceName = "code_frame_sequence_gen", allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "text", length = 1023)
    private String text;

    @Column(name = "tag")
    private String tag;

    @ManyToOne
    @ToString.Exclude
    @JoinColumn(name = "user_id")
    private User user;

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {return false;}
        CodeFrame other = (CodeFrame) o;
        return id != null & id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
