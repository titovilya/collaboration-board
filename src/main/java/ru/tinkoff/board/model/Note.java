package ru.tinkoff.board.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

@Entity(name = "Note")
@SuperBuilder
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="fintech_notes",
        indexes = {@Index(name = "note_tag_index", columnList = "tag")}
)
@NamedEntityGraph(
        name = "note-entity-graph",
        attributeNodes = {
                @NamedAttributeNode(value = "referencedNotes", subgraph = "referencedSub"),
                @NamedAttributeNode(value = "referencingNotes", subgraph = "referencingSub")
        },
        subgraphs = {
                @NamedSubgraph(name = "referencedSub",
                attributeNodes = {
                        @NamedAttributeNode("referencedNotes"),
                        @NamedAttributeNode("referencingNotes")
                }),
                @NamedSubgraph(name = "referencingSub",
                attributeNodes = {
                        @NamedAttributeNode("referencingNotes"),
                        @NamedAttributeNode("referencedNotes")
                })
        }
)
public class Note extends BaseForm {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "note_sequence_gen")
    @SequenceGenerator(name = "note_sequence_gen", sequenceName = "note_sequence_gen", allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "tag")
    private String tag;

    @Column(name = "text", length = 1023)
    private String text;

    @ManyToMany(cascade = CascadeType.PERSIST, mappedBy = "referencingNotes")
    private List<Note> referencedNotes;

    @JoinTable(name = "fintech_referencing_notes",
            joinColumns = {@JoinColumn(name = "referenced_note_id")},
            inverseJoinColumns = {@JoinColumn(name = "referencing_note_id")})
    @ManyToMany(cascade = CascadeType.PERSIST)
    @ToString.Exclude
    private List<Note> referencingNotes;

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {return false;}
        Note other = (Note) o;
        return id != null & id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
