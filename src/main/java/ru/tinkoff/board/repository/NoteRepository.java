package ru.tinkoff.board.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.model.Note;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface NoteRepository extends CrudRepository<Note, Long> {

    boolean existsById(Long id);

    @Override
    @EntityGraph(value = "note-entity-graph")
    Optional<Note> findById(Long aLong);

    List<Note> findAll();

    @Transactional
    @EntityGraph(value = "note-entity-graph")
    void deleteById(Long id);

    List<Note> findByIdIn(List<Long> ids);

    Integer countAllByIdIn(Set<Long> ids);

    default boolean existsAllById(Set<Long> ids) {
        return countAllByIdIn(ids).equals(ids.size());
    }

    List<Note> findByTag(String tag);

    // for every put request to note
    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT n FROM Note n WHERE n.id = :id")
    Optional<Note> findByIdWithLock(Long id);
}
