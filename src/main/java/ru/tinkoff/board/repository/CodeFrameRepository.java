package ru.tinkoff.board.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.model.CodeFrame;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;

@Repository
public interface CodeFrameRepository extends CrudRepository<CodeFrame, Long> {
    boolean existsById(Long id);

    @Override
    @EntityGraph(value = "code-frame-entity-graph")
    Optional<CodeFrame> findById(Long aLong);

    @EntityGraph(value = "code-frame-entity-graph")
    List<CodeFrame> findAll();

    @Transactional
    @EntityGraph(value = "code-frame-entity-graph")
    void deleteById(Long id);

    @EntityGraph(value = "code-frame-entity-graph")
    List<CodeFrame> findByTag(String tag);

    // for every put request to code frame
    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT c FROM CodeFrame c WHERE c.id = :id")
    Optional<CodeFrame> findByIdWithLock(Long id);
}
