package ru.tinkoff.board.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.model.Task;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {

    boolean existsById(Long id);

    @Override
    @EntityGraph(value = "task-entity-graph")
    Optional<Task> findById(Long aLong);

    @EntityGraph(value = "task-entity-graph")
    List<Task> findAll();

    @Transactional
    @EntityGraph(value = "task-entity-graph")
    void deleteById(Long id);

    @EntityGraph(value = "task-entity-graph")
    List<Task> findByBoard_Id(Long id);

    List<Task> findByIdIn(List<Long> ids);

    @EntityGraph(value = "task-entity-graph")
    List<Task> findByTag(String tag);

    Integer countAllByIdIn(Set<Long> ids);

    default boolean existsAllById(Set<Long> ids) {
        return countAllByIdIn(ids).equals(ids.size());
    }

    // for every put request to task
    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT t FROM Task t WHERE t.id = :id")
    Optional<Task> findByIdWithLock(Long id);
}
