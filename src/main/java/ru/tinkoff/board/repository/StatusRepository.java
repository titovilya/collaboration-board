package ru.tinkoff.board.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.model.Status;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface StatusRepository extends CrudRepository<Status, Long> {

    boolean existsById(Long id);

    @Override
    @EntityGraph(value = "status-entity-graph")
    Optional<Status> findById(Long aLong);

    @EntityGraph(value = "status-entity-graph")
    List<Status> findAll();

    @Transactional
    @EntityGraph(value = "status-entity-graph")
    void deleteById(Long id);

    @EntityGraph(value = "status-entity-graph")
    List<Status> findByIdIn(List<Long> ids);

    @EntityGraph(value = "status-entity-graph")
    Integer countAllByIdIn(Set<Long> ids);

    @EntityGraph(value = "status-entity-graph")
    default boolean existsAllById(Set<Long> ids) {
        return countAllByIdIn(ids).equals(ids.size());
    }

    // for every put request to status
    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT s FROM Status s WHERE s.id = :id")
    Optional<Status> findByIdWithLock(Long id);
}
