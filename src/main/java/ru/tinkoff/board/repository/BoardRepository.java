package ru.tinkoff.board.repository;

import lombok.NonNull;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.model.Board;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;

@Repository
public interface BoardRepository extends CrudRepository<Board, Long> {

    @Override
    @EntityGraph(value = "board-entity-graph")
    Optional<Board> findById(@NonNull Long aLong);

    boolean existsById(Long id);

    @EntityGraph(value = "board-entity-graph")
    List<Board> findAll();

    @Transactional
    @EntityGraph(value = "board-entity-graph")
    void deleteById(Long id);

    // for every put request to board
    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT b FROM Board b WHERE b.id = :id")
    Optional<Board> findByIdWithLock(Long id);
}
