package ru.tinkoff.board.service;

import lombok.NonNull;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.api.dto.CodeFrameDto;
import ru.tinkoff.board.model.CodeFrame;

import java.util.List;

public interface CodeFrameService {

    CodeFrame findByIdWithLock(@NonNull Long id);

    CodeFrameDto.Response.Public findByIdDto(@NonNull Long id);

    List<CodeFrameDto.Response.Public> findAllDtos();

    @Transactional
    void deleteById(@NonNull Long id);

    @Transactional
    CodeFrameDto.Response.Public saveDto(@NonNull CodeFrameDto.Request.Create codeFrameDto);

    @Transactional
    CodeFrameDto.Response.Public updateDto(@NonNull Long id, @NonNull CodeFrameDto.Request.Create codeFrameDto);

    List<CodeFrameDto.Response.Public> findByTag(String tag);
}
