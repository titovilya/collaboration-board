package ru.tinkoff.board.service;

import lombok.NonNull;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.api.dto.BoardDto;
import ru.tinkoff.board.model.Board;

import java.util.List;

public interface BoardService {

    Board findById(@NonNull Long id);

    BoardDto.Response.Public findByIdDto(@NonNull Long id);

    List<BoardDto.Response.Public> findAllDtos();

    @Transactional
    void deleteById(@NonNull Long id);

    @Transactional
    BoardDto.Response.Public saveDto(@NonNull BoardDto.Request.Create boardDto);

    @Transactional
    void updateDto(@NonNull Long id, @NonNull BoardDto.Request.Update boardDto);

    @Transactional
    void migrateTasks(@NonNull Long idFrom, @NonNull Long idTo);
}
