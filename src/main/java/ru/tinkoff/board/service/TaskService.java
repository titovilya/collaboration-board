package ru.tinkoff.board.service;

import lombok.NonNull;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.api.dto.TaskDto;
import ru.tinkoff.board.model.Task;

import java.util.List;
import java.util.Set;

public interface TaskService {

    Task findByIdWithLock(@NonNull Long id);

    TaskDto.Response.Public findByIdDto(@NonNull Long id);

    List<TaskDto.Response.Public> findAllDtos();

    @Transactional
    void deleteById(@NonNull Long id);

    @Transactional
    TaskDto.Response.Public saveDto(@NonNull TaskDto.Request.Create taskDto);

    @Transactional
    TaskDto.Response.Public updateDto(@NonNull Long id, @NonNull TaskDto.Request.Create taskDto);

    List<Task> findByTaskDtoIds(List<TaskDto.Request.CreateAsRelation> dtos);

    List<Task> findByBoardId(Long id);

    boolean existAllByIds(Set<Long> ids);

    List<TaskDto.Response.Public> findByTag(String tag);

    @Transactional
    void saveAll(List<Task> tasks);
}
