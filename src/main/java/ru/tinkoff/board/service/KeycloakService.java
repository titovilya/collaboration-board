package ru.tinkoff.board.service;

import lombok.NonNull;
import org.keycloak.representations.idm.UserRepresentation;
import ru.tinkoff.board.api.dto.UserDto;

import javax.ws.rs.core.Response;
import java.util.List;

public interface KeycloakService {

    void deleteUserViaUsername(@NonNull String username);

    List<UserRepresentation> findByUsername(String username);

    Response createFromStudentDto(UserDto.Request.Create userDto);

    void updateUserFromDto(UserRepresentation fromUser, UserDto.Request.Create userDto);
}
