package ru.tinkoff.board.service.serviceImpl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.api.dto.UserDto;
import ru.tinkoff.board.mapper.UserMapper;
import ru.tinkoff.board.model.User;
import ru.tinkoff.board.repository.UserRepository;
import ru.tinkoff.board.security.exception.BadRequestCustomRuntimeException;
import ru.tinkoff.board.security.exception.NotFoundCustomRuntimeException;
import ru.tinkoff.board.service.KeycloakService;
import ru.tinkoff.board.service.UserService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    private final KeycloakService keycloakService;

    private boolean existById(Long id) {
        return  userRepository.existsById(id);
    }

    @Override
    public User findById(@NonNull Long id) {
        return userRepository.findById(id).orElseThrow(
                () -> new NotFoundCustomRuntimeException(String.format("%s [%s] was not found", User.class.getName(), id))
        );
    }

    private User findByIdWithLock(@NonNull Long id) {
        return userRepository.findByIdWithLock(id).orElseThrow(
                () -> new NotFoundCustomRuntimeException(String.format("%s [%s] was not found", User.class.getName(), id))
        );
    }

    @Override
    public UserDto.Response.Public findByIdDto(@NonNull Long id) {
        return userMapper.toDtoResponse(findById(id));
    }

    private List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public List<UserDto.Response.Public> findAllDtos() {
        return userMapper.listToDtoResponse(findAll());
    }

    @Override
    @Transactional
    public void deleteById(@NonNull Long id) {
        if (!existById(id)) {
            final String msg = String.format("%s [%s] was not found", User.class.getName(), id);
            throw new NotFoundCustomRuntimeException(msg);
        }
        User user = findById(id);
        userRepository.deleteById(id);
        keycloakService.deleteUserViaUsername(user.getUsername());
    }

    @Transactional
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public UserDto.Response.Public saveDto(@NonNull UserDto.Request.Create userDto) {
        checkIfUserAlreadyExistInKeycloak(userDto); // must be checked before saving to database
        User user = save(userMapper.toEntity(userDto));
        keycloakService.createFromStudentDto(userDto);
        return userMapper.toDtoResponse(user);
    }

    private void checkIfUserAlreadyExistInKeycloak(UserDto.Request.Create userDto) {
        List<UserRepresentation> users = keycloakService.findByUsername(userDto.getUsername());
        throwIfUserAlreadyExistInKeycloak(users, userDto);
    }

    private void throwIfUserAlreadyExistInKeycloak(List<UserRepresentation> users, UserDto.Request.Create userDto) {
        if (users.size() > 0) {
            final String msg = String.format("%s [%s] username already exists in keycloak.", User.class.getName(), userDto.getUsername());
            throw new BadRequestCustomRuntimeException(msg);
        }
    }

    @Override
    @Transactional
    public UserDto.Response.Public updateDto(@NonNull Long id, @NonNull UserDto.Request.Create userDto) {
        List<UserRepresentation> users = keycloakService.findByUsername(userDto.getUsername());
        throwIfUserNotExistInKeycloak(users, userDto); //must be checked before saving to database

        User userWithId = save(getMergedUser(id, userDto));
        updateKeycloakUser(users, userDto);
        return userMapper.toDtoResponse(userWithId);
    }

    private User getMergedUser(Long id, UserDto.Request.Create userDto) {
        User foundUser = findByIdWithLock(id);
        User userFromDto = userMapper.toEntity(userDto);
        return userMapper.mergeEntities(foundUser, userFromDto);
    }

    private void updateKeycloakUser(List<UserRepresentation> users, UserDto.Request.Create userDto) {
        keycloakService.updateUserFromDto(users.get(0), userDto);
    }

    private void throwIfUserNotExistInKeycloak(List<UserRepresentation> users, UserDto.Request.Create userDto) {
        if (users.size() == 0) {
            final String msg = String.format("%s [%s] username is not exists in keycloak.", User.class.getName(), userDto.getUsername());
            throw new BadRequestCustomRuntimeException(msg);
        }
    }
}
