package ru.tinkoff.board.service.serviceImpl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.api.dto.CodeFrameDto;
import ru.tinkoff.board.mapper.CodeFrameMapper;
import ru.tinkoff.board.model.CodeFrame;
import ru.tinkoff.board.model.User;
import ru.tinkoff.board.repository.CodeFrameRepository;
import ru.tinkoff.board.security.exception.BadRequestCustomRuntimeException;
import ru.tinkoff.board.security.exception.NotFoundCustomRuntimeException;
import ru.tinkoff.board.service.CodeFrameService;
import ru.tinkoff.board.service.UserService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CodeFrameServiceImpl implements CodeFrameService {

    private final CodeFrameRepository codeFrameRepository;

    private final CodeFrameMapper codeFrameMapper;

    private final UserService userService;

    private boolean existById(Long id) {
        return  codeFrameRepository.existsById(id);
    }

    private CodeFrame findById(Long id) {
        return codeFrameRepository.findById(id).orElseThrow(
                () -> new NotFoundCustomRuntimeException(String.format("%s [%s] was not found", CodeFrame.class.getName(), id))
        );
    }

    @Override
    public CodeFrame findByIdWithLock(Long id) {
        return codeFrameRepository.findByIdWithLock(id).orElseThrow(
                () -> new NotFoundCustomRuntimeException(String.format("%s [%s] was not found", CodeFrame.class.getName(), id))
        );
    }

    @Override
    public CodeFrameDto.Response.Public findByIdDto(@NonNull Long id) {
        return codeFrameMapper.toDtoResponse(findById(id));
    }

    private List<CodeFrame> findAll() {
        return codeFrameRepository.findAll();
    }

    @Override
    public List<CodeFrameDto.Response.Public> findAllDtos() {
        return codeFrameMapper.listToDtoResponse(findAll());
    }

    @Override
    public void deleteById(@NonNull Long id) {
        if (!existById(id)) {
            final String msg = String.format("%s [%s] was not found", CodeFrame.class.getName(), id);
            throw new NotFoundCustomRuntimeException(msg);
        }
        codeFrameRepository.deleteById(id);
    }

    @Transactional
    public CodeFrame save(@NonNull CodeFrame codeFrame) {
        return codeFrameRepository.save(codeFrame);
    }

    @Override
    public CodeFrameDto.Response.Public saveDto(@NonNull CodeFrameDto.Request.Create codeFrameDto) {
        CodeFrame codeFrame = addUser(codeFrameDto);
        CodeFrame codeFrameWithId = save(codeFrame);
        return codeFrameMapper.toDtoResponse(codeFrameWithId);
    }

    @Override
    public CodeFrameDto.Response.Public updateDto(@NonNull Long id, @NonNull CodeFrameDto.Request.Create codeFrameDto) {
        CodeFrame updatedCodeFrame = getMergedCodeFrame(id, codeFrameDto);
        CodeFrame savedNoteWithId = save(updatedCodeFrame);
        return codeFrameMapper.toDtoResponse(savedNoteWithId);
    }

    private CodeFrame getMergedCodeFrame(Long id, CodeFrameDto.Request.Create codeFrameDto) {
        CodeFrame codeFrameWithUser = addUser(codeFrameDto);
        CodeFrame foundCodeFrame = findByIdWithLock(id);
        return codeFrameMapper.mergeEntities(foundCodeFrame, codeFrameWithUser);
    }

    @Override
    public List<CodeFrameDto.Response.Public> findByTag(String tag) {
        List<CodeFrame> codeFrames = codeFrameRepository.findByTag(tag);
        return codeFrameMapper.listToDtoResponse(codeFrames);
    }

    private CodeFrame addUser(@NonNull CodeFrameDto.Request.Create codeFrameDto) {
        CodeFrame codeFrame = codeFrameMapper.toEntity(codeFrameDto);

        getUserFromCodeFrameDto(codeFrameDto).ifPresent(codeFrame::setUser);
        return codeFrame;
    }

    private Optional<User> getUserFromCodeFrameDto(CodeFrameDto.Request.Create codeFrameDto) {
        if (codeFrameDto.getUser() != null && codeFrameDto.getUser().getId() != null) {
            Long userId = codeFrameDto.getUser().getId();
            return Optional.of(userService.findById(userId));
        }
        return Optional.empty();
    }
}
