package ru.tinkoff.board.service.serviceImpl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.api.dto.StatusDto;
import ru.tinkoff.board.mapper.StatusMapper;
import ru.tinkoff.board.model.Board;
import ru.tinkoff.board.model.Status;
import ru.tinkoff.board.repository.StatusRepository;
import ru.tinkoff.board.security.exception.BadRequestCustomRuntimeException;
import ru.tinkoff.board.security.exception.NotFoundCustomRuntimeException;
import ru.tinkoff.board.service.BoardService;
import ru.tinkoff.board.service.StatusService;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StatusServiceImpl implements StatusService {

    private final StatusRepository statusRepository;

    private final StatusMapper statusMapper;

    private final BoardService boardService;

    private boolean existById(Long id) {
        return statusRepository.existsById(id);
    }

    @Override
    public Status findById(@NonNull Long id) {
        return statusRepository.findById(id).orElseThrow(
                () -> new NotFoundCustomRuntimeException(String.format("%s [%s] was not found", Status.class.getName(), id))
        );
    }

    private Status findByIdWithLock(@NonNull Long id) {
        return statusRepository.findByIdWithLock(id).orElseThrow(
                () -> new NotFoundCustomRuntimeException(String.format("%s [%s] was not found", Status.class.getName(), id))
        );
    }

    @Override
    public StatusDto.Response.Public findByIdDto(@NonNull Long id) {
        return statusMapper.toDtoResponse(findById(id));
    }

    private List<Status> findAll() {
        return statusRepository.findAll();
    }

    @Override
    public List<StatusDto.Response.Public> findAllDtos() {
        return statusMapper.listToDtoResponse(findAll());
    }

    @Override
    @Transactional
    public void deleteById(@NonNull Long id) {
        if (!existById(id)) {
            final String msg = String.format("%s [%s] was not found", Status.class.getName(), id);
            throw new NotFoundCustomRuntimeException(msg);
        }
        statusRepository.deleteById(id);
    }

    @Override
    public Status copyStatus(@NonNull Status status) {
        StatusDto.Response.Copy dto = statusMapper.toDtoCopy(status);
        return statusMapper.toEntity(dto);
    }

    @Transactional
    public Status save(@NonNull Status status) {
        return statusRepository.save(status);
    }

    @Override
    public void saveAll(List<Status> statuses) {
        statusRepository.saveAll(statuses);
    }

    @Override
    public boolean existsAllByIds(Set<Long> ids) {
        return statusRepository.existsAllById(ids);
    }

    @Override
    @Transactional
    public StatusDto.Response.Public saveDto(@NonNull StatusDto.Request.Create statusDto) {
        Status statusWithBoard = addBoard(statusDto);
        Status statusWithId = save(statusWithBoard);
        return statusMapper.toDtoResponse(statusWithId);
    }

    @Override
    public StatusDto.Response.Public updateDto(@NonNull Long id, @NonNull StatusDto.Request.Create statusDto) {
        Status updatedStatus = getMergedStatus(id, statusDto);
        Status statusWithId = save(updatedStatus);
        return statusMapper.toDtoResponse(statusWithId);
    }

    private Status getMergedStatus(Long id, StatusDto.Request.Create statusDto) {
        Status foundStatus = findByIdWithLock(id);
        Status statusWithBoard = addBoard(statusDto);
        return statusMapper.mergeEntities(foundStatus, statusWithBoard);
    }

    @Override
    public List<Status> findByStatusDtoIds(List<StatusDto.Request.CreateAsRelation> dtos) {
        List<Long> ids = dtos.stream().map(StatusDto.Request.CreateAsRelation::getId).collect(Collectors.toList());
        return statusRepository.findByIdIn(ids);
    }

    private Status addBoard(@NonNull StatusDto.Request.Create statusDto) {
        Status status = statusMapper.toEntity(statusDto);
        getBoardFromStatusDto(statusDto).ifPresent(status::setBoard);
        return status;
    }

    private Optional<Board> getBoardFromStatusDto(StatusDto.Request.Create statusDto) {
        if (statusDto.getBoard() != null && statusDto.getBoard().getId() != null) {
            Long id = statusDto.getBoard().getId();
            return Optional.of(boardService.findById(id));
        }
        return Optional.empty();
    }
}
