package ru.tinkoff.board.service.serviceImpl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.api.dto.BoardDto;
import ru.tinkoff.board.api.dto.StatusDto;
import ru.tinkoff.board.api.dto.TaskDto;
import ru.tinkoff.board.mapper.BoardMapper;
import ru.tinkoff.board.model.Board;
import ru.tinkoff.board.model.Status;
import ru.tinkoff.board.model.Task;
import ru.tinkoff.board.repository.BoardRepository;
import ru.tinkoff.board.security.exception.BadRequestCustomRuntimeException;
import ru.tinkoff.board.security.exception.NotFoundCustomRuntimeException;
import ru.tinkoff.board.service.BoardService;
import ru.tinkoff.board.service.StatusService;
import ru.tinkoff.board.service.TaskService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardServiceImpl implements BoardService {

    private final BoardRepository boardRepository;

    private final BoardMapper boardMapper;

    private TaskService taskService;

    private StatusService statusService;

    @Autowired
    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    @Autowired
    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    private boolean existById(Long id) {
        return boardRepository.existsById(id);
    }

    @Override
    public Board findById(@NonNull Long id) {
        return boardRepository.findById(id).orElseThrow(
                () -> new NotFoundCustomRuntimeException(String.format("%s [%s] was not found", Board.class.getName(), id))
        );
    }

    private Board findByIdWithLock(@NonNull Long id) {
        return boardRepository.findByIdWithLock(id).orElseThrow(
                () -> new NotFoundCustomRuntimeException(String.format("%s [%s] was not found", Board.class.getName(), id))
        );
    }

    @Override
    public BoardDto.Response.Public findByIdDto(@NonNull Long id) {
        return boardMapper.toDtoResponse(findById(id));
    }

    private List<Board> findAll() {
        return boardRepository.findAll();
    }

    @Override
    public List<BoardDto.Response.Public> findAllDtos() {
        return boardMapper.listToDtoResponse(findAll());
    }

    @Override
    @Transactional
    public void deleteById(@NonNull Long id) {
        if (!existById(id)) {
            final String msg = String.format("%s [%s] was not found", Board.class.getName(), id);
            throw new NotFoundCustomRuntimeException(msg);
        }
        boardRepository.deleteById(id);
    }

    @Transactional
    public Board save(@NonNull Board board) {
        return boardRepository.save(board);
    }

    @Override
    @Transactional
    public BoardDto.Response.Public saveDto(@NonNull BoardDto.Request.Create boardDto) {
        Board board = save(boardMapper.toEntity(boardDto));
        return boardMapper.toDtoResponse(board);
    }

    @Override
    @Transactional
    public void updateDto(@NonNull Long id, @NonNull BoardDto.Request.Update boardDto) {
        Board updatedBoard = getMergedBoard(id, boardDto);
        save(updatedBoard);
    }

    private Board getMergedBoard(Long id, BoardDto.Request.Update boardDto) {
        Board foundBoard = findByIdWithLock(id);
        Board boardWithStatusesAndTasks = addStatusesAndTasks(id, boardDto);
        return boardMapper.mergeEntities(foundBoard, boardWithStatusesAndTasks);
    }

    @Override
    @Transactional
    public void migrateTasks(@NonNull Long idFrom, @NonNull Long idTo) {
        if (!existById(idFrom)) {
            final String msg = String.format("%s [%s] was not found", Board.class.getName(), idFrom);
            throw new NotFoundCustomRuntimeException(msg);
        }
        Board boardTo = findById(idTo);
        List<Task> tasksFrom = taskService.findByBoardId(idFrom);
        List<Task> updatedTasks = changeStatusOfListTasks(tasksFrom, boardTo);
        taskService.saveAll(updatedTasks);
    }
    
    private List<Task> changeStatusOfListTasks(List<Task> tasksFrom, Board boardTo) {
        return tasksFrom.stream()
                .map((task) -> changeStatusAndBoardOfTask(task, boardTo))
                .toList();
    }

    private Task changeStatusAndBoardOfTask(Task task, Board boardTo) {
        task.setBoard(boardTo);
        if (task.getStatus() != null) {
            Status status = task.getStatus();
            Status copied = statusService.copyStatus(status);
            copied.setBoard(boardTo);
            task.setStatus(copied);
        }
        return task;
    }

    private Board addStatusesAndTasks(Long id, BoardDto.Request.Update boardDto) {
        Board board = boardMapper.toEntity(boardDto);

        List<Task> tasks = addTasks(id, boardDto);
        if (tasks != null) {
            board.setTasks(tasks);
        }

        List<Status> statuses = addStatuses(id, boardDto);
        if (statuses != null) {
            board.setStatuses(statuses);
        }

        return board;
    }

    private List<Task> addTasks(Long id, BoardDto.Request.Update boardDto) {
        List<TaskDto.Request.CreateAsRelation> taskDtos = boardDto.getTasks();
        return extractTasksFromBoardDto(id, taskDtos);
    }

    private List<Status> addStatuses(Long id, BoardDto.Request.Update boardDto) {
        List<StatusDto.Request.CreateAsRelation> statusDtos = boardDto.getStatuses();
        return extractStatusesFromBoardDto(id, statusDtos);
    }

    private List<Task> extractTasksFromBoardDto(Long id, List<TaskDto.Request.CreateAsRelation> taskDtos) {
        if (taskDtos != null && taskDtos.size() > 0) {
            List<Task> tasks = taskService.findByTaskDtoIds(taskDtos);
            boolean isTasksConnectedToBoard = tasks.stream().allMatch(task -> task.getBoard().getId().equals(id));
            throwIfRelationsNotAssociatedWithBoard(isTasksConnectedToBoard);
            return tasks;
        }
        return null;
    }

    private List<Status> extractStatusesFromBoardDto(Long id, List<StatusDto.Request.CreateAsRelation> statusDtos) {
        if (statusDtos != null && statusDtos.size() > 0) {
            List<Status> statuses = statusService.findByStatusDtoIds(statusDtos);
            boolean isStatusesConnectedToBoard = statuses.stream().allMatch(status -> status.getBoard().getId().equals(id));
            throwIfRelationsNotAssociatedWithBoard(isStatusesConnectedToBoard);
            return statuses;
        }
        return null;
    }

    private void throwIfRelationsNotAssociatedWithBoard(boolean isConnectedToBoard) {
        if (!isConnectedToBoard) {
            throw new BadRequestCustomRuntimeException("Can`t add relation attribute, which connected to another board.");
        }
    }
}
