package ru.tinkoff.board.service.serviceImpl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.api.dto.NoteDto;
import ru.tinkoff.board.mapper.NoteMapper;
import ru.tinkoff.board.model.Note;
import ru.tinkoff.board.repository.NoteRepository;
import ru.tinkoff.board.security.exception.BadRequestCustomRuntimeException;
import ru.tinkoff.board.security.exception.NotFoundCustomRuntimeException;
import ru.tinkoff.board.service.NoteService;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class NoteServiceImpl implements NoteService {

    private final NoteRepository noteRepository;

    private final NoteMapper noteMapper;

    private boolean existById(Long id) {
        return noteRepository.existsById(id);
    }

    private Note findById(Long id) {
        return noteRepository.findById(id).orElseThrow(
                () -> new NotFoundCustomRuntimeException(String.format("%s [%s] was not found", Note.class.getName(), id))
        );
    }

    @Override
    public Note findByIdWithLock(Long id) {
        return noteRepository.findByIdWithLock(id).orElseThrow(
                () -> new NotFoundCustomRuntimeException(String.format("%s [%s] was not found", Note.class.getName(), id))
        );
    }

    @Override
    public NoteDto.Response.Public findByIdDto(@NonNull Long id) {
        return noteMapper.toDtoResponse(findById(id));
    }

    private List<Note> findAll() {
        return noteRepository.findAll();
    }

    @Override
    public List<NoteDto.Response.Public> findAllDtos() {
        return noteMapper.listToDtoResponse(findAll());
    }

    @Override
    @Transactional
    public void deleteById(@NonNull Long id) {
        if (!existById(id)) {
            final String msg = String.format("%s [%s] was not found", Note.class.getName(), id);
            throw new NotFoundCustomRuntimeException(msg);
        }
        noteRepository.deleteById(id);
    }

    @Override
    public boolean existsAllByIds(Set<Long> ids) {
        return noteRepository.existsAllById(ids);
    }

    @Transactional
    public Note save(@NonNull Note note) {
        return noteRepository.save(note);
    }

    @Override
    @Transactional
    public NoteDto.Response.Public saveDto(@NonNull NoteDto.Request.Create noteDto) {
        Note noteWithReferencableNotes = addReferencableNotes(noteDto);
        Note noteWithId = save(noteWithReferencableNotes);
        return noteMapper.toDtoResponse(noteWithId);
    }

    @Override
    @Transactional
    public NoteDto.Response.Public updateDto(@NonNull Long id, @NonNull NoteDto.Request.Create noteDto) {
        Note updatedNote = getMergedNotes(id, noteDto);
        Note noteWithId = save(updatedNote);
        return noteMapper.toDtoResponse(noteWithId);
    }

    private Note getMergedNotes(Long id, NoteDto.Request.Create noteDto) {
        Note foundNote = findByIdWithLock(id);
        Note noteWithReferencableNotes = addReferencableNotes(noteDto);
        return noteMapper.mergeEntities(foundNote, noteWithReferencableNotes);
    }

    private Note addReferencableNotes(@NonNull NoteDto.Request.Create noteDto) {
        Note note = noteMapper.toEntity(noteDto);

        getReferencedNotes(noteDto).ifPresent(note::setReferencedNotes);
        getReferencingNotes(noteDto).ifPresent(note::setReferencingNotes);
        return note;
    }

    private Optional<List<Note>> getReferencedNotes(NoteDto.Request.Create noteDto) {
        if (noteDto.getReferencedNotes() != null) {
            return Optional.of(findNoteByNoteDtoIds(noteDto.getReferencedNotes()));
        }
        return Optional.empty();
    }

    private Optional<List<Note>> getReferencingNotes(NoteDto.Request.Create noteDto) {
        if (noteDto.getReferencingNotes() != null) {
            return Optional.of(findNoteByNoteDtoIds(noteDto.getReferencingNotes()));
        }
        return Optional.empty();
    }

    @Override
    public List<Note> findNoteByNoteDtoIds(List<NoteDto.Request.CreateAsRelation> notes) {
        List<Long> ids = notes.stream().map(NoteDto.Request.CreateAsRelation::getId).collect(Collectors.toList());
        return noteRepository.findByIdIn(ids);
    }

    @Override
    public List<NoteDto.Response.Public> findByTag(String tag) {
        return noteMapper.listToDtoResponse(noteRepository.findByTag(tag));
    }
}
