package ru.tinkoff.board.service.serviceImpl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.api.dto.TaskDto;
import ru.tinkoff.board.mapper.TaskMapper;
import ru.tinkoff.board.model.Board;
import ru.tinkoff.board.model.Status;
import ru.tinkoff.board.model.Task;
import ru.tinkoff.board.model.User;
import ru.tinkoff.board.repository.TaskRepository;
import ru.tinkoff.board.security.exception.BadRequestCustomRuntimeException;
import ru.tinkoff.board.security.exception.NotFoundCustomRuntimeException;
import ru.tinkoff.board.service.BoardService;
import ru.tinkoff.board.service.StatusService;
import ru.tinkoff.board.service.TaskService;
import ru.tinkoff.board.service.UserService;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;

    private final UserService userService;

    private final BoardService boardService;

    private final StatusService statusService;

    private final TaskMapper taskMapper;

    private boolean existById(Long id) {
        return taskRepository.existsById(id);
    }

    private Task findById(Long id) {
        return taskRepository.findById(id).orElseThrow(
                () -> new NotFoundCustomRuntimeException(String.format("%s [%s] was not found", Task.class.getName(), id))
        );
    }

    @Override
    public Task findByIdWithLock(Long id) {
        return taskRepository.findByIdWithLock(id).orElseThrow(
                () -> new NotFoundCustomRuntimeException(String.format("%s [%s] was not found", Task.class.getName(), id))
        );
    }

    @Override
    public TaskDto.Response.Public findByIdDto(@NonNull Long id) {
        return taskMapper.toDtoResponse(findById(id));
    }

    private List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<TaskDto.Response.Public> findAllDtos() {
        return taskMapper.listToDtoResponse(findAll());
    }

    @Override
    @Transactional
    public void deleteById(@NonNull Long id) {
        if (!existById(id)) {
            final String msg = String.format("%s [%s] was not found", Task.class.getName(), id);
            throw new NotFoundCustomRuntimeException(msg);
        }
        taskRepository.deleteById(id);
    }

    @Transactional
    public Task save(@NonNull Task task) {
        return taskRepository.save(task);
    }

    @Override
    @Transactional
    public TaskDto.Response.Public saveDto(@NonNull TaskDto.Request.Create taskDto) {
        Task taskWithStatusAndBoardAndUser = addStatusBoardUser(taskDto);
        Task taskWithId = save(taskWithStatusAndBoardAndUser);
        return taskMapper.toDtoResponse(taskWithId);
    }

    @Override
    @Transactional
    public TaskDto.Response.Public updateDto(@NonNull Long id, @NonNull TaskDto.Request.Create taskDto) {
        Task updatedTask = getMergedTask(id, taskDto);
        Task taskWithId = save(updatedTask);
        return taskMapper.toDtoResponse(taskWithId);
    }

    private Task getMergedTask(Long id, TaskDto.Request.Create taskDto) {
        Task foundTask = findByIdWithLock(id);
        Task taskWithStatusAndBoardAndUser = addStatusBoardUser(taskDto);
        return taskMapper.mergeEntities(foundTask, taskWithStatusAndBoardAndUser);
    }

    @Override
    public List<Task> findByTaskDtoIds(List<TaskDto.Request.CreateAsRelation> dtos) {
        List<Long> ids = dtos.stream().map(TaskDto.Request.CreateAsRelation::getId).collect(Collectors.toList());
        return taskRepository.findByIdIn(ids);
    }

    @Override
    public List<Task> findByBoardId(Long id) {
        return taskRepository.findByBoard_Id(id);
    }

    @Override
    public boolean existAllByIds(Set<Long> ids) {
        return taskRepository.existsAllById(ids);
    }

    @Override
    public List<TaskDto.Response.Public> findByTag(String tag) {
        return taskMapper.listToDtoResponse(taskRepository.findByTag(tag));
    }

    @Override
    @Transactional
    public void saveAll(List<Task> tasks) {
        taskRepository.saveAll(tasks);
    }

    private Task addStatusBoardUser(TaskDto.Request.Create taskDto) {
        Task task = taskMapper.toEntity(taskDto);

        getStatus(taskDto).ifPresent(task::setStatus);
        getBoard(taskDto).ifPresent(task::setBoard);
        getUser(taskDto).ifPresent(task::setUser);
        return task;
    }

    private Optional<Status> getStatus(TaskDto.Request.Create taskDto) {
        if (taskDto.getStatus() != null && taskDto.getStatus().getId() != null) {
            Long statusId = taskDto.getStatus().getId();
            return Optional.of(statusService.findById(statusId));
        }
        return Optional.empty();
    }

    private Optional<Board> getBoard(TaskDto.Request.Create taskDto) {
        if (taskDto.getBoard() != null && taskDto.getBoard().getId() != null) {
            Long boardId = taskDto.getBoard().getId();
            return Optional.of(boardService.findById(boardId));
        }
        return Optional.empty();
    }

    private Optional<User> getUser(TaskDto.Request.Create taskDto) {
        if (taskDto.getUser() != null && taskDto.getUser().getId() != null) {
            Long userId = taskDto.getUser().getId();
            return Optional.of(userService.findById(userId));
        }
        return Optional.empty();
    }
}
