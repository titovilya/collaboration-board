package ru.tinkoff.board.service.keycloak;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.tinkoff.board.api.dto.UserDto;
import ru.tinkoff.board.api.dto.keycloak.UserKeycloakRequest;
import ru.tinkoff.board.security.exception.BadRequestCustomRuntimeException;
import ru.tinkoff.board.service.KeycloakService;

import javax.ws.rs.core.Response;
import java.util.List;

@Service
@RequiredArgsConstructor
public class KeycloakServiceImpl implements KeycloakService {

    private final Keycloak keycloak;
    @Value("${keycloak.realm}")
    private String realm;

    public List<UserRepresentation> findAll() {
        return keycloak
                .realm(realm)
                .users()
                .list();
    }

    @Override
    public List<UserRepresentation> findByUsername(String username) {
        return keycloak
                .realm(realm)
                .users()
                .search(username);
    }

    public UserRepresentation findById(String id) {
        return keycloak
                .realm(realm)
                .users()
                .get(id)
                .toRepresentation();
    }

    public Response create(UserKeycloakRequest request) {
        var password = preparePasswordRepresentation(request.getPassword());
        var user = prepareUserRepresentation(request, password);
        Response response = keycloak
                .realm(realm)
                .users()
                .create(user);
        if (response.getStatus() != 201) {
            throw new BadRequestCustomRuntimeException("User was not created");
        }
        return response;
    }

    @Override
    public Response createFromStudentDto(UserDto.Request.Create userDto) {
        UserKeycloakRequest user = new UserKeycloakRequest();
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        return create(user);
    }

    public void updateUser(UserRepresentation userByUsername, UserKeycloakRequest request) {
        var password = preparePasswordRepresentation(request.getPassword());
        var user = prepareUserRepresentation(request, password);
        if (userByUsername == null || userByUsername.getId() == null) {
            throw new BadRequestCustomRuntimeException("Username is not exists");
        }
        keycloak
                .realm(realm)
                .users()
                .get(userByUsername.getId())
                .update(user);
    }

    @Override
    public void updateUserFromDto(UserRepresentation fromUser, UserDto.Request.Create userDto) {
        UserKeycloakRequest user = new UserKeycloakRequest();
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        updateUser(fromUser, user);
    }

    public void deleteUser(@NonNull String id) {
        keycloak
                .realm(realm)
                .users()
                .get(id)
                .remove();
    }

    @Override
    public void deleteUserViaUsername(@NonNull String username) {
        UserRepresentation userByUsername = findByUsername(username).get(0);
        if (userByUsername == null || userByUsername.getId() == null) {
            throw new BadRequestCustomRuntimeException("Username is not exists");
        }
        String id = userByUsername.getId();
        deleteUser(id);
    }

    private UserRepresentation prepareUserRepresentation(UserKeycloakRequest request, CredentialRepresentation cR) {
        var newUser = new UserRepresentation();
        newUser.setUsername(request.getUsername());
        newUser.setCredentials(List.of(cR));
        newUser.setEnabled(true);
        return newUser;
    }

    private CredentialRepresentation preparePasswordRepresentation(String password) {
        var cR = new CredentialRepresentation();
        cR.setTemporary(false);
        cR.setType(CredentialRepresentation.PASSWORD);
        cR.setValue(password);
        return cR;
    }
}

