package ru.tinkoff.board.service;

import lombok.NonNull;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.api.dto.NoteDto;
import ru.tinkoff.board.model.Note;

import java.util.List;
import java.util.Set;

public interface NoteService {

    Note findByIdWithLock(@NonNull Long id);

    NoteDto.Response.Public findByIdDto(@NonNull Long id);

    List<NoteDto.Response.Public> findAllDtos();

    @Transactional
    void deleteById(@NonNull Long id);

    boolean existsAllByIds(Set<Long> ids);

    @Transactional
    NoteDto.Response.Public saveDto(@NonNull NoteDto.Request.Create noteDto);

    @Transactional
    NoteDto.Response.Public updateDto(@NonNull Long id, @NonNull NoteDto.Request.Create noteDto);

    List<Note> findNoteByNoteDtoIds(List<NoteDto.Request.CreateAsRelation> notes);

    List<NoteDto.Response.Public> findByTag(String tag);
}
