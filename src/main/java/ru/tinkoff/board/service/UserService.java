package ru.tinkoff.board.service;

import lombok.NonNull;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.api.dto.UserDto;
import ru.tinkoff.board.model.User;

import java.util.List;

public interface UserService {

    UserDto.Response.Public findByIdDto(@NonNull Long id);

    List<UserDto.Response.Public> findAllDtos();

    @Transactional
    void deleteById(@NonNull Long id);

    @Transactional
    UserDto.Response.Public saveDto(@NonNull UserDto.Request.Create userDto);

    @Transactional
    UserDto.Response.Public updateDto(@NonNull Long id, @NonNull UserDto.Request.Create userDto);

    User findById(@NonNull Long id);
}
