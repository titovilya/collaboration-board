package ru.tinkoff.board.service;

import lombok.NonNull;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.board.api.dto.StatusDto;
import ru.tinkoff.board.model.Status;

import java.util.List;
import java.util.Set;

public interface StatusService {

    Status findById(@NonNull Long id);

    StatusDto.Response.Public findByIdDto(@NonNull Long id);

    List<StatusDto.Response.Public> findAllDtos();

    @Transactional
    void deleteById(@NonNull Long id);

    @Transactional
    StatusDto.Response.Public saveDto(@NonNull StatusDto.Request.Create statusDto);

    @Transactional
    StatusDto.Response.Public updateDto(@NonNull Long id, @NonNull StatusDto.Request.Create statusDto);

    List<Status> findByStatusDtoIds(List<StatusDto.Request.CreateAsRelation> Dtos);

    void saveAll(List<Status> statuses);

    boolean existsAllByIds(Set<Long> ids);

    Status copyStatus(@NonNull Status status);
}
