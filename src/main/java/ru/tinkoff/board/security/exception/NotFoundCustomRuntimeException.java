package ru.tinkoff.board.security.exception;

import java.io.Serial;

public class NotFoundCustomRuntimeException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 1L;

    private final String message;

    public NotFoundCustomRuntimeException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
