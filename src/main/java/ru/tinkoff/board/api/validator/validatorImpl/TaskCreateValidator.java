package ru.tinkoff.board.api.validator.validatorImpl;

import lombok.RequiredArgsConstructor;
import ru.tinkoff.board.api.dto.TaskDto;
import ru.tinkoff.board.api.validator.ValidateTaskCreate;
import ru.tinkoff.board.model.Status;
import ru.tinkoff.board.service.StatusService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

@RequiredArgsConstructor
public class TaskCreateValidator implements ConstraintValidator<ValidateTaskCreate, TaskDto.Request.Create> {

    private final StatusService statusService;

    @Override
    public boolean isValid(TaskDto.Request.Create dto, ConstraintValidatorContext constraintValidatorContext) {
        return checkIfStatusesInTaskAndBoardTheSame(dto);
    }

    private boolean checkIfStatusesInTaskAndBoardTheSame(TaskDto.Request.Create dto) {
        Optional<Long> dtoBoardId = getBoardIdFromDto(dto);
        Optional<Long> statusBoardIdInDatabase = getBoardIdOfStatusFromDatabase(dto);
        if (dtoBoardId.isPresent() && statusBoardIdInDatabase.isPresent()) {
            return compareStatusesIdsOfTaskAndBoard(dtoBoardId.get(), statusBoardIdInDatabase.get());
        }
        return true;
    }

    private Optional<Long> getBoardIdFromDto(TaskDto.Request.Create dto) {
        if (dto.getBoard() != null && dto.getBoard().getId() != null) {
            return Optional.of(dto.getBoard().getId());
        }
        return Optional.empty();
    }

    private Optional<Long> getBoardIdOfStatusFromDatabase(TaskDto.Request.Create dto) {
        if (dto.getStatus() != null && dto.getStatus().getId() != null) {
            Status status = statusService.findById(dto.getStatus().getId());
            if (extractBoardIdFromStatus(status).isPresent()) {
                return Optional.of(extractBoardIdFromStatus(status).get());
            }
        }
        return Optional.empty();
    }

    private Optional<Long> extractBoardIdFromStatus(Status status) {
        if (status != null && status.getBoard() != null && status.getBoard().getId() != null) {
            return Optional.of(status.getBoard().getId());
        }
        return Optional.empty();
    }

    private boolean compareStatusesIdsOfTaskAndBoard(Long statusDatabaseBoardId, Long taskDtoBoardId) {
        return statusDatabaseBoardId.equals(taskDtoBoardId);
    }
}
