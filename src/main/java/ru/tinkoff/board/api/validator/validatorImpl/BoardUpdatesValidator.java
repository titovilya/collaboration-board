package ru.tinkoff.board.api.validator.validatorImpl;

import lombok.RequiredArgsConstructor;
import ru.tinkoff.board.api.dto.BoardDto;
import ru.tinkoff.board.api.dto.StatusDto;
import ru.tinkoff.board.api.dto.TaskDto;
import ru.tinkoff.board.api.validator.ValidateBoardUpdates;
import ru.tinkoff.board.service.StatusService;
import ru.tinkoff.board.service.TaskService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class BoardUpdatesValidator implements ConstraintValidator<ValidateBoardUpdates, BoardDto.Request.Update> {

    private final StatusService statusService;

    private final TaskService taskService;

    @Override
    public boolean isValid(BoardDto.Request.Update dto, ConstraintValidatorContext constraintValidatorContext) {
        return checkBothStatusAndTaskExist(dto);
    }

    private boolean checkBothStatusAndTaskExist(BoardDto.Request.Update dto) {
        boolean isStatusesExist = isEveryStatusExists(dto);
        boolean isTasksExist = isEveryTaskExists(dto);
        return isStatusesExist & isTasksExist;
    }

    private boolean isEveryStatusExists(BoardDto.Request.Update dto) {
        boolean isStatusesExist = true;
        if (dto.getStatuses() != null) {
            Set<Long> statusIds = dto.getStatuses().stream().map(StatusDto.Request.CreateAsRelation::getId).collect(Collectors.toSet());
            isStatusesExist = checkStatusesIdsExistsInDatabase(statusIds);
        }
        return isStatusesExist;
    }

    private boolean checkStatusesIdsExistsInDatabase(Set<Long> statusesIds) {
        if (statusesIds.size() > 0) {
            return statusService.existsAllByIds(statusesIds);
        }
        return true;
    }

    private boolean isEveryTaskExists(BoardDto.Request.Update dto) {
        boolean isTasksExist = true;
        if (dto.getTasks() != null) {
            Set<Long> tasksIds = dto.getTasks().stream().map(TaskDto.Request.CreateAsRelation::getId).collect(Collectors.toSet());
            isTasksExist = checkTasksIdsExistInDatabase(tasksIds);
        }
        return isTasksExist;
    }

    private boolean checkTasksIdsExistInDatabase(Set<Long> tasksIds) {
        if (tasksIds.size() > 0) {
            return taskService.existAllByIds(tasksIds);
        }
        return true;
    }
}
