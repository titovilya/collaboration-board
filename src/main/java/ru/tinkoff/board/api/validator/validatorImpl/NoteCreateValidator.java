package ru.tinkoff.board.api.validator.validatorImpl;

import lombok.RequiredArgsConstructor;
import ru.tinkoff.board.api.dto.NoteDto;
import ru.tinkoff.board.api.validator.ValidateNoteCreate;
import ru.tinkoff.board.service.NoteService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class NoteCreateValidator implements ConstraintValidator<ValidateNoteCreate, NoteDto.Request.Create> {

    private final NoteService noteService;

    @Override
    public boolean isValid(NoteDto.Request.Create dto, ConstraintValidatorContext constraintValidatorContext) {
        return checkIfRelatedNoteExist(dto);
    }

    private boolean checkIfRelatedNoteExist(NoteDto.Request.Create dto) {
        Set<Long> notes = getNotesWithRelatedNotes(dto);
        if (notes.size() > 0) {
            return noteService.existsAllByIds(notes);
        }
        return true;
    }

    private Set<Long> getNotesWithRelatedNotes(NoteDto.Request.Create dto) {
        Set<Long> referencedNotes = getReferencedNotes(dto);
        Set<Long> referencingNotes = getReferencingNotes(dto);
        return mergeNotesTogether(referencedNotes, referencingNotes);
    }

    private Set<Long> mergeNotesTogether(Set<Long> referencedNotes, Set<Long> referencingNotes) {
        Set<Long> notes = new HashSet<>();
        notes.addAll(referencedNotes);
        notes.addAll(referencingNotes);
        return notes;
    }

    private Set<Long> getReferencedNotes(NoteDto.Request.Create dto) {
        Set<Long> referencedNotes = new HashSet<>();
        if (dto.getReferencedNotes() != null) {
            referencedNotes = dto.getReferencedNotes().stream().map(NoteDto.Request.CreateAsRelation::getId).collect(Collectors.toSet());
        }
        return referencedNotes;
    }

    private Set<Long> getReferencingNotes(NoteDto.Request.Create dto) {
        Set<Long> referencingNotes = new HashSet<>();
        if (dto.getReferencingNotes() != null) {
            referencingNotes = dto.getReferencingNotes().stream().map(NoteDto.Request.CreateAsRelation::getId).collect(Collectors.toSet());
        }
        return referencingNotes;
    }
}
