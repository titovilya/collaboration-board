package ru.tinkoff.board.api.validator;

import ru.tinkoff.board.api.validator.validatorImpl.TaskCreateValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE,
        ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = TaskCreateValidator.class)
public @interface ValidateTaskCreate {

    String message() default "Board must have the same status, which connected to this task.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
