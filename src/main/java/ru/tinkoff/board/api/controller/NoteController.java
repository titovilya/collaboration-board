package ru.tinkoff.board.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.board.api.dto.NoteDto;
import ru.tinkoff.board.service.NoteService;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/notes")
public class NoteController {

    private final NoteService noteService;

    @RolesAllowed({"app-admin", "app-user"})
    @GetMapping("/{id}")
    public NoteDto.Response.Public getNote(@PathVariable Long id) {
        return noteService.findByIdDto(id);
    }

    @RolesAllowed({"app-admin", "app-user"})
    @GetMapping
    public List<NoteDto.Response.Public> getNotes() {
        return noteService.findAllDtos();
    }

    @RolesAllowed({"app-admin"})
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public NoteDto.Response.Public createNote(@RequestBody @Valid NoteDto.Request.Create noteDto) {
        return noteService.saveDto(noteDto);
    }

    @RolesAllowed({"app-admin"})
    @DeleteMapping("/{id}")
    public void deleteNote(@PathVariable Long id) {
        noteService.deleteById(id);
    }

    @RolesAllowed({"app-admin"})
    @PutMapping("/{id}")
    public NoteDto.Response.Public updateNote(@PathVariable Long id,
                                              @RequestBody @Valid NoteDto.Request.Create noteDto) {
        return noteService.updateDto(id, noteDto);
    }

    @RolesAllowed({"app-admin", "app-user"})
    @GetMapping("/tag")
    public List<NoteDto.Response.Public> findNoteByTag(@RequestParam String tag) {
        return noteService.findByTag(tag);
    }
}
