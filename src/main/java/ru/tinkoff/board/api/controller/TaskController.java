package ru.tinkoff.board.api.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.board.api.dto.TaskDto;
import ru.tinkoff.board.service.TaskService;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/tasks")
public class TaskController {

    private final TaskService taskService;

    @RolesAllowed({"app-admin", "app-user"})
    @GetMapping("/{id}")
    public TaskDto.Response.Public getTask(@PathVariable Long id) {
        return taskService.findByIdDto(id);
    }

    @RolesAllowed({"app-admin", "app-user"})
    @GetMapping
    public List<TaskDto.Response.Public> getTasks() {
        return taskService.findAllDtos();
    }

    @RolesAllowed({"app-admin"})
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TaskDto.Response.Public createTask(@RequestBody @Valid TaskDto.Request.Create taskDto) {
        return taskService.saveDto(taskDto);
    }

    @RolesAllowed({"app-admin"})
    @DeleteMapping("/{id}")
    public void deleteTask(@PathVariable Long id) {
        taskService.deleteById(id);
    }

    @RolesAllowed({"app-admin"})
    @PutMapping("/{id}")
    public TaskDto.Response.Public updateTask(@PathVariable Long id,
                                              @RequestBody @Valid TaskDto.Request.Create taskDto) {
        return taskService.updateDto(id, taskDto);
    }

    @RolesAllowed({"app-admin", "app-user"})
    @GetMapping("/tag")
    public List<TaskDto.Response.Public> findTaskByTag(@RequestParam String tag) {
        return taskService.findByTag(tag);
    }
}
