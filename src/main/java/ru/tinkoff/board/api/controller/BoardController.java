package ru.tinkoff.board.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.board.api.dto.BoardDto;
import ru.tinkoff.board.service.BoardService;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/boards")
public class BoardController {

    private final BoardService boardService;

    @RolesAllowed({"app-admin", "app-user"})
    @GetMapping("/{id}")
    public BoardDto.Response.Public getBoard(@PathVariable Long id) {
        return boardService.findByIdDto(id);
    }

    @RolesAllowed({"app-admin", "app-user"})
    @GetMapping
    public List<BoardDto.Response.Public> getBoards() {
        return boardService.findAllDtos();
    }

    @RolesAllowed({"app-admin"})
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BoardDto.Response.Public createBoard(@RequestBody @Valid BoardDto.Request.Create boardDto) {
        return boardService.saveDto(boardDto);
    }

    @RolesAllowed({"app-admin"})
    @PostMapping("/migrate-tasks")
    public void migrateTasksFromBoard(@RequestParam Long idFrom, @RequestParam Long idTo) {
        boardService.migrateTasks(idFrom, idTo);
    }

    @RolesAllowed({"app-admin"})
    @DeleteMapping("/{id}")
    public void deleteBoard(@PathVariable Long id) {
        boardService.deleteById(id);
    }

    @RolesAllowed({"app-admin"})
    @PutMapping("/{id}")
    public void updateBoard(@PathVariable Long id,
                                                    @RequestBody @Valid BoardDto.Request.Update boardDto) {
        boardService.updateDto(id, boardDto);
    }
}
