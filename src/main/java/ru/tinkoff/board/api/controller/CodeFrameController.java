package ru.tinkoff.board.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.board.api.dto.CodeFrameDto;
import ru.tinkoff.board.service.CodeFrameService;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/code-frames")
public class CodeFrameController {

    private final CodeFrameService codeFrameService;

    @RolesAllowed({"app-admin", "app-user"})
    @GetMapping("/{id}")
    public CodeFrameDto.Response.Public getCodeFrame(@PathVariable Long id) {
        return codeFrameService.findByIdDto(id);
    }

    @RolesAllowed({"app-admin", "app-user"})
    @GetMapping
    public List<CodeFrameDto.Response.Public> getCodeFrames() {
        return codeFrameService.findAllDtos();
    }

    @RolesAllowed({"app-admin"})
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CodeFrameDto.Response.Public createCodeFrame(@RequestBody @Valid CodeFrameDto.Request.Create codeFrameDto) {
        return codeFrameService.saveDto(codeFrameDto);
    }

    @RolesAllowed({"app-admin"})
    @DeleteMapping("/{id}")
    public void deleteCodeFrame(@PathVariable Long id) {
        codeFrameService.deleteById(id);
    }

    @RolesAllowed({"app-admin"})
    @PutMapping("/{id}")
    public CodeFrameDto.Response.Public updateCodeFrame(@PathVariable Long id,
                                                        @RequestBody @Valid CodeFrameDto.Request.Create codeFrameDto) {
        return codeFrameService.updateDto(id, codeFrameDto);
    }

    @RolesAllowed({"app-admin", "app-user"})
    @GetMapping("/tag")
    public List<CodeFrameDto.Response.Public> findCodeFrameByTag(@RequestParam String tag) {
        return codeFrameService.findByTag(tag);
    }
}
