package ru.tinkoff.board.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.board.api.dto.StatusDto;
import ru.tinkoff.board.service.StatusService;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/statuses")
public class StatusController {

    private final StatusService statusService;

    @RolesAllowed({"app-admin", "app-user"})
    @GetMapping("/{id}")
    public StatusDto.Response.Public getStatus(@PathVariable Long id) {
        return statusService.findByIdDto(id);
    }

    @RolesAllowed({"app-admin", "app-user"})
    @GetMapping
    public List<StatusDto.Response.Public> getStatuses() {
        return statusService.findAllDtos();
    }

    @RolesAllowed({"app-admin"})
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public StatusDto.Response.Public createStatus(@RequestBody @Valid StatusDto.Request.Create statusDto) {
        return statusService.saveDto(statusDto);
    }

    @RolesAllowed({"app-admin"})
    @DeleteMapping("/{id}")
    public void deleteStatus(@PathVariable Long id) {
        statusService.deleteById(id);
    }

    @RolesAllowed({"app-admin"})
    @PutMapping("/{id}")
    public StatusDto.Response.Public updateStatus(@PathVariable Long id,
                                                  @RequestBody @Valid StatusDto.Request.Create statusDto) {
        return statusService.updateDto(id, statusDto);
    }
}
