package ru.tinkoff.board.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Value;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

import static ru.tinkoff.board.api.dto.BaseFormDto.CreatedOn;
import static ru.tinkoff.board.api.dto.BaseFormDto.UpdatedOn;

public enum CodeFrameDto {;

    private interface Name { @NotBlank(message = "Name must be not empty and not null") String getName(); }
    private interface Id { @NotNull(message = "Id must be not null") Long getId();}
    private interface Tag { String getTag(); }
    private interface Text { String getText(); }
    private interface User {
        UserDto.Response.Public getUser();}
    private interface UserRelation {
        UserDto.Request.CreateAsRelation getUser();}

    public enum Request {;
        public static class CreateAsRelation implements Id {
            Long id;
            public CreateAsRelation() {}
            public CreateAsRelation(Long id) {
                this.id = id;
            }

            @Override
            public Long getId() {
                return this.id;
            }
        }
        @Value
        public static class Create implements Name, Tag, Text, UserRelation {
            String name;
            String tag;
            String text;
            UserDto.Request.CreateAsRelation user;
        }
    }
    public enum Response {;
        @Value
        public static class Public implements Id, Name, Tag, Text, CreatedOn, UpdatedOn, User {
            Long id;
            String name;
            String tag;
            String text;
            LocalDateTime createdOn;
            LocalDateTime updatedOn;
            UserDto.Response.Public user;
        }
    }
}
