package ru.tinkoff.board.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Value;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tinkoff.board.api.validator.ValidateTaskCreate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

import static ru.tinkoff.board.api.dto.BaseFormDto.CreatedOn;
import static ru.tinkoff.board.api.dto.BaseFormDto.UpdatedOn;

public enum TaskDto {;
    private interface Name { @NotBlank(message = "Name must be not empty and not null") String getName(); }
    private interface Id { @NotNull(message = "Id must be not null") Long getId();}
    private interface Tag { String getTag(); }
    private interface Description { @Size(max = 1023, message = "Description is too long") String getDescription(); }
    private interface Status { StatusDto.Response.Public getStatus(); }
    private interface StatusRelation { StatusDto.Request.CreateAsRelation getStatus(); }
    private interface BoardRelation { @NotNull(message = "Board must be not null") BoardDto.Request.CreateAsRelation getBoard(); }
    private interface Board { @NotNull(message = "Board must be not null") BoardDto.Response.Short getBoard(); }
    private interface User {
        UserDto.Response.Public getUser();}
    private interface UserRelation {
        UserDto.Request.CreateAsRelation getUser();}

    public enum Request {;
        public static class CreateAsRelation implements Id {
            Long id;
            public CreateAsRelation() {}
            public CreateAsRelation(Long id) {
                this.id = id;
            }

            @Override
            public Long getId() {
                return this.id;
            }
        }

        @Value
        @ValidateTaskCreate
        public static class Create implements Name, Tag, Description, StatusRelation, BoardRelation, UserRelation {
            String name;
            String tag;
            String description;
            StatusDto.Request.CreateAsRelation status;
            BoardDto.Request.CreateAsRelation board;
            UserDto.Request.CreateAsRelation user;
        }
    }

    public enum Response {;
        @Value
        public static class Public implements Id, Name, Tag, Description, CreatedOn, UpdatedOn, Board, Status, User {
            Long id;
            String name;
            String tag;
            String description;
            LocalDateTime createdOn;
            LocalDateTime updatedOn;
            BoardDto.Response.Short board;
            StatusDto.Response.Public status;
            UserDto.Response.Public user;

        }

        @Value
        public static class Short implements Id {
            Long id;
        }
    }
}
