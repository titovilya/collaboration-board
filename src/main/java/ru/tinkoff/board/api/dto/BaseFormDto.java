package ru.tinkoff.board.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

public class BaseFormDto {
    public interface CreatedOn { @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss") @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss") LocalDateTime getCreatedOn(); }
    public interface UpdatedOn { @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss") @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss") LocalDateTime getUpdatedOn(); }
}
