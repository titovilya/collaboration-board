package ru.tinkoff.board.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Value;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tinkoff.board.api.dto.BaseFormDto.CreatedOn;
import ru.tinkoff.board.api.dto.BaseFormDto.UpdatedOn;
import ru.tinkoff.board.api.validator.ValidateNoteCreate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

public enum NoteDto {;

    private interface Name { @NotBlank(message = "Name must be not empty and not null") String getName(); }
    private interface Id { @NotNull(message = "Id must be not null") Long getId();}
    private interface Tag { String getTag(); }
    private interface Text { String getText(); }
    private interface ReferencedNotes { List<NoteDto.Request.CreateAsRelation> getReferencedNotes(); }
    private interface ReferencingNotes { List<NoteDto.Request.CreateAsRelation> getReferencingNotes(); }
    private interface ReferencedNotesShort { List<NoteDto.Response.Short> getReferencedNotes();}
    private interface ReferencingNotesShort { List<NoteDto.Response.Short> getReferencingNotes();}

    public enum Request {;

        public static class CreateAsRelation implements Id {
            Long id;
            public CreateAsRelation() {}
            public CreateAsRelation(Long id) {
                this.id = id;
            }

            @Override
            public Long getId() {
                return this.id;
            }
        }

        @Value
        @ValidateNoteCreate
        public static class Create implements Name, Tag, Text, ReferencedNotes, ReferencingNotes {
            String name;
            String tag;
            String text;
            List<NoteDto.Request.CreateAsRelation> referencedNotes;
            List<NoteDto.Request.CreateAsRelation> referencingNotes;
        }
    }
    public enum Response {;
        @Value
        public static class Short implements Id {
            Long id;
        }

        @Value
        public static class Public implements Id, Name, Tag, Text, CreatedOn, UpdatedOn, ReferencedNotesShort, ReferencingNotesShort {
            Long id;
            String name;
            String tag;
            String text;
            LocalDateTime createdOn;
            LocalDateTime updatedOn;
            List<NoteDto.Response.Short> referencedNotes;
            List<NoteDto.Response.Short> referencingNotes;
        }
    }
}
