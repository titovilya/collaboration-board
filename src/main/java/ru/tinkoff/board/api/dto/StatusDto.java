package ru.tinkoff.board.api.dto;

import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public enum StatusDto {;

    private interface Name { @NotBlank(message = "Name must be not empty and not null") String getName(); }
    private interface Id { @NotNull Long getId();}
    private interface BoardRelation { @NotNull BoardDto.Request.CreateAsRelation getBoard(); }
    private interface Board { BoardDto.Response.Short getBoard(); }

    public enum Request {;
        @Value
        public static class Create implements Name, BoardRelation {
            String name;
            BoardDto.Request.CreateAsRelation board;
        }

        public static class CreateAsRelation implements Id {
            Long id;
            public CreateAsRelation() {}
            public CreateAsRelation(Long id) {
                this.id = id;
            }

            @Override
            public Long getId() {
                return this.id;
            }
        }
    }

    public enum Response {;
        @Value
        public static class Public implements Id, Name, Board {
            Long id;
            String name;
            BoardDto.Response.Short board;
        }

        @Value
        public static class Copy implements Name, Board {
            String name;
            BoardDto.Response.Short board;
        }

        @Value
        public static class Short implements Id {
            Long id;
        }
    }
}
