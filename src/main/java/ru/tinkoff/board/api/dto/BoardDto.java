package ru.tinkoff.board.api.dto;

import lombok.Value;
import ru.tinkoff.board.api.dto.BaseFormDto.CreatedOn;
import ru.tinkoff.board.api.dto.BaseFormDto.UpdatedOn;
import ru.tinkoff.board.api.validator.ValidateBoardUpdates;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

public enum BoardDto {;

    private interface Name { @NotBlank(message = "Name must be not empty and not null") String getName(); }
    private interface Id { @NotNull Long getId();}
    private interface Description { @Size(max = 1023, message = "Description is too long") String getDescription(); }
    private interface Statuses { List<StatusDto.Response.Short> getStatuses(); }
    private interface StatusesRelation { List<StatusDto.Request.CreateAsRelation> getStatuses(); }
    private interface TasksRelation { List<TaskDto.Request.CreateAsRelation> getTasks(); }
    private interface Tasks { List<TaskDto.Response.Short> getTasks(); }

    public enum Request {;
        @Value
        public static class Create implements Name, Description {
            String name;
            String description;
        }

        @Value
        @ValidateBoardUpdates
        public static class Update implements Name, Description,TasksRelation, StatusesRelation {
            String name;
            String description;
            List<TaskDto.Request.CreateAsRelation> tasks;
            List<StatusDto.Request.CreateAsRelation> statuses;
        }

        public static class CreateAsRelation implements Id {
            Long id;
            public CreateAsRelation() {}
            public CreateAsRelation(Long id) {
                this.id = id;
            }

            @Override
            public Long getId() {
                return this.id;
            }
        }
    }

    public enum Response {;
        @Value
        public static class Public implements Id, Name, Description, CreatedOn, UpdatedOn, Tasks, Statuses {
            Long id;
            String name;
            String description;
            LocalDateTime createdOn;
            LocalDateTime updatedOn;
            List<TaskDto.Response.Short> tasks;
            List<StatusDto.Response.Short> statuses;
        }

        @Value
        public static class Short implements Id {
            Long id;
        }
    }


}
