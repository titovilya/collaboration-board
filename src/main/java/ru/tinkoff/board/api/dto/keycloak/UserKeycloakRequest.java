package ru.tinkoff.board.api.dto.keycloak;

import lombok.Data;

@Data
public class UserKeycloakRequest {
    private String username;
    private String password;
}

