package ru.tinkoff.board.api.dto;

import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public enum UserDto {;

    private interface Id { @NotNull(message = "Id must be not null") Long getId();}
    private interface Email {String getEmail();}
    private interface Username { @NotBlank(message = "Username must be not empty and not null") String getUsername(); }
    private interface Password { @NotBlank(message = "Password must be not empty and not null") String getPassword();}
    private interface Name { String getName(); }

    public enum Request {;
        public static class CreateAsRelation implements Id {
            Long id;
            public CreateAsRelation() {}
            public CreateAsRelation(Long id) {
                this.id = id;
            }

            @Override
            public Long getId() {
                return this.id;
            }
        }

        @Value
        public static class Create implements Username, Password, Name, Email {
            String username;
            String password;
            String name;
            String email;
        }
    }
    public enum Response {;
        @Value
        public static class Public implements Id, Username, Name, Email {
            Long id;
            String username;
            String name;
            String email;
        }
    }
}
