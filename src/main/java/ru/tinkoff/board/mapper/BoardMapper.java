package ru.tinkoff.board.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import ru.tinkoff.board.api.dto.BoardDto;
import ru.tinkoff.board.model.Board;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface BoardMapper {

    BoardDto.Response.Public toDtoResponse(Board board);

    List<BoardDto.Response.Public> listToDtoResponse(List<Board> boards);

    Board toEntity(BoardDto.Request.Create boardDto);

    Board toEntity(BoardDto.Request.Update boardDto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Board mergeEntities(@MappingTarget Board foundBoard, Board newBoard);
}
