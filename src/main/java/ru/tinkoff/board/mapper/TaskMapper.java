package ru.tinkoff.board.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import ru.tinkoff.board.api.dto.TaskDto;
import ru.tinkoff.board.model.Task;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface TaskMapper {

    TaskDto.Response.Public toDtoResponse(Task task);

    List<TaskDto.Response.Public> listToDtoResponse(List<Task> tasks);

    Task toEntity(TaskDto.Request.Create taskDto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Task mergeEntities(@MappingTarget Task foundTasks, Task newTask);
}
