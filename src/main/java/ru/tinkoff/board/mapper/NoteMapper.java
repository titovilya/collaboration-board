package ru.tinkoff.board.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import ru.tinkoff.board.api.dto.NoteDto;
import ru.tinkoff.board.model.Note;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface NoteMapper {

    NoteDto.Response.Public toDtoResponse(Note note);

    List<NoteDto.Response.Public> listToDtoResponse(List<Note> notes);

    Note toEntity(NoteDto.Request.Create noteDto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Note mergeEntities(@MappingTarget Note foundNote, Note newNote);
}
