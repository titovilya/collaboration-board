package ru.tinkoff.board.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import ru.tinkoff.board.api.dto.CodeFrameDto;
import ru.tinkoff.board.model.CodeFrame;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface CodeFrameMapper {

    CodeFrameDto.Response.Public toDtoResponse(CodeFrame codeFrame);

    List<CodeFrameDto.Response.Public> listToDtoResponse(List<CodeFrame> codeFrames);

    CodeFrame toEntity(CodeFrameDto.Request.Create codeFrameDto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    CodeFrame mergeEntities(@MappingTarget CodeFrame codeFrame, CodeFrame newCodeFrame);
}
