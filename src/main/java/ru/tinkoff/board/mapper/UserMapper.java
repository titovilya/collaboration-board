package ru.tinkoff.board.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import ru.tinkoff.board.api.dto.UserDto;
import ru.tinkoff.board.model.User;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

    UserDto.Response.Public toDtoResponse(User user);

    List<UserDto.Response.Public> listToDtoResponse(List<User> users);

    User toEntity(UserDto.Request.Create userDto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    User mergeEntities(@MappingTarget User foundUser, User newUser);
}
