package ru.tinkoff.board.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import ru.tinkoff.board.api.dto.StatusDto;
import ru.tinkoff.board.model.Status;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface StatusMapper {

    StatusDto.Response.Public toDtoResponse(Status status);

    StatusDto.Response.Copy toDtoCopy(Status status);

    List<StatusDto.Response.Public> listToDtoResponse(List<Status> statuses);

    Status toEntity(StatusDto.Request.Create statusDto);

    Status toEntity(StatusDto.Response.Copy statusDto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Status mergeEntities(@MappingTarget Status foundStatus, Status newStatus);
}
