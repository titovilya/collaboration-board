package ru.tinkoff.board;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.lifecycle.Startables;
import org.testcontainers.utility.DockerImageName;
import org.testcontainers.utility.MountableFile;

import java.time.Duration;
import java.util.Map;
import java.util.stream.Stream;

@SpringBootTest("spring-hibernate-query-utils.n-plus-one-queries-detection.error-level=INFO")
@TestPropertySource("/application.properties")
@DirtiesContext
public class ApplicationIntegrationTest {

    public static PostgreSQLContainer<?> postgresContainer = new PostgreSQLContainer<>("postgres:14.3-alpine");

    public static GenericContainer<?> keycloak =
            new GenericContainer<>(DockerImageName.parse("jboss/keycloak:16.1.1"))
                    .waitingFor(Wait.forHttp("/auth").forStatusCode(200).withStartupTimeout(Duration.ofSeconds(180)))
                    .withExposedPorts(8080)
                    .withCopyFileToContainer(MountableFile.forClasspathResource("/keycloak/realm-export.json"), "/opt/jboss/keycloak/imports/realm-export.json")
                    .withCommand("-b 0.0.0.0 -Dkeycloak.profile.feature.upload_scripts=enabled -Dkeycloak.import=/opt/jboss/keycloak/imports/realm-export.json")
                    .withEnv(Map.of(
                            "KEYCLOAK_USER", "admin",
                            "KEYCLOAK_PASSWORD", "admin",
                            "DB_VENDOR", "h2"
                    ));

    static {
        Startables.deepStart(Stream.of(postgresContainer)).join();
        System.setProperty("spring.datasource.url", postgresContainer.getJdbcUrl());
        System.setProperty("spring.datasource.username", postgresContainer.getUsername());
        System.setProperty("spring.datasource.password", postgresContainer.getPassword());
        keycloak.start();
        System.setProperty("kc.port", keycloak.getMappedPort(8080).toString());
    }

    @Autowired
    private Flyway flyway;

    public void cleanAndMigrate() {
        flyway.clean();
        flyway.migrate();
    }

}
