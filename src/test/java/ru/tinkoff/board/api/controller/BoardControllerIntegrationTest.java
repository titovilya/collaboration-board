package ru.tinkoff.board.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yannbriancon.interceptor.HibernateQueryInterceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.tinkoff.board.ApplicationIntegrationTest;
import ru.tinkoff.board.api.dto.BoardDto;
import ru.tinkoff.board.security.exception.NotFoundCustomRuntimeException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
public class BoardControllerIntegrationTest extends ApplicationIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private HibernateQueryInterceptor hibernateQueryInterceptor;

    private String pathUrl = "/boards";

    @AfterEach
    void tearDown() {
        cleanAndMigrate();
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void getBoardWithUserRole() throws Exception {
        String id = "1";
        hibernateQueryInterceptor.startQueryCount();
        String result = this.mockMvc.perform(get(pathUrl +"/" + id))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertThat(result).contains("\"id\":"+id);
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(2);
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void getBoardWithAdminRole() throws Exception {
        String id = "1";
        hibernateQueryInterceptor.startQueryCount();
        String result = this.mockMvc.perform(get(pathUrl + "/" + id))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertThat(result).contains("\"id\":"+id);
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(2);
    }

    @Test
    void getBoardAccessDeniedException() throws Exception {
        String id = "1";
        this.mockMvc.perform(get(pathUrl + "/" + id))
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }

    @Test
    @WithMockUser(roles = {"app-admin", "app-user"})
    void getBoardNotExistException() throws Exception {
        String id = "20000001";
        this.mockMvc.perform(get(pathUrl + "/" + id))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(NotFoundCustomRuntimeException.class));
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void getBoardsAsUserRole() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        String result = this.mockMvc.perform(get(pathUrl))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertThat(result).contains("\"id\":1");
        assertThat(result).contains("\"id\":2");
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(4);
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void getBoardsAsAdminRole() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        String result = this.mockMvc.perform(get(pathUrl))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertThat(result).contains("\"id\":1");
        assertThat(result).contains("\"id\":2");
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(4);
    }

    @Test
    void getBoardsAccessDeniedException() throws Exception {
        this.mockMvc.perform(get(pathUrl))
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void createBoardAsAdminRole() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        BoardDto.Request.Create dto = new BoardDto.Request.Create("createBoardAsAdminRole", "createBoardAsAdminRole");
        String requestCreate = objectMapper.writeValueAsString(dto);
        String response = this.mockMvc.perform(post(pathUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestCreate)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();
        assertThat(response).contains("\"name\":\"createBoardAsAdminRole\"");
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(2);
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void createBoardAsUserRoleAccessDeniedException() throws Exception {
        BoardDto.Request.Create dto = new BoardDto.Request.Create("createBoardAsAdminRole", "createBoardAsAdminRole");
        String requestCreate = objectMapper.writeValueAsString(dto);
        this.mockMvc.perform(post(pathUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestCreate)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void deleteBoardAsAdminRole() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        this.mockMvc.perform(delete(pathUrl + "/3"))
                .andExpect(status().isOk());
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(7);//cascade = CascadeType.ALL, orphanRemoval = true
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void deleteBoardAsUserRoleAccessDeniedException() throws Exception {
        this.mockMvc.perform(delete(pathUrl + "/3"))
                .andExpect(status().isUnauthorized())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void deleteBoardAsAdminRoleNotFoundException() throws Exception {
        this.mockMvc.perform(delete( pathUrl + "/200000001"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void updateBoardAsAdminRole() throws Exception{
        hibernateQueryInterceptor.startQueryCount();
        BoardDto.Request.Update dto = new BoardDto.Request.Update("updated", "createBoardAsAdminRole", null, null);
        String requestUpdate = objectMapper.writeValueAsString(dto);
        this.mockMvc.perform(put(pathUrl + "/3")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestUpdate)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(2);
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void updateBoardAsUserRoleAccessDeniedException() throws Exception{
        BoardDto.Request.Update dto = new BoardDto.Request.Update("updated", "createBoardAsAdminRole", null, null);
        String requestUpdate = objectMapper.writeValueAsString(dto);
        this.mockMvc.perform(put(pathUrl + "/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestUpdate)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void migrateTaskFromBoardTest() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        this.mockMvc.perform(post(pathUrl + "/migrate-tasks")
                .param("idFrom", "1")
                .param("idTo", "2")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(10);
    }
}
