package ru.tinkoff.board.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yannbriancon.interceptor.HibernateQueryInterceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.tinkoff.board.ApplicationIntegrationTest;
import ru.tinkoff.board.api.dto.BoardDto;
import ru.tinkoff.board.api.dto.StatusDto;
import ru.tinkoff.board.security.exception.NotFoundCustomRuntimeException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
public class StatusControllerIntegrationTest extends ApplicationIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private HibernateQueryInterceptor hibernateQueryInterceptor;

    private String pathUrl = "/statuses";

    @AfterEach
    void tearDown() {
        cleanAndMigrate();
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void getStatusWithUserRole() throws Exception {
        String id = "1";
        hibernateQueryInterceptor.startQueryCount();
        String result = this.mockMvc.perform(get(pathUrl +"/" + id))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertThat(result).contains("\"id\":"+id);
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(1);
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void getStatusWithAdminRole() throws Exception {
        String id = "1";
        hibernateQueryInterceptor.startQueryCount();
        String result = this.mockMvc.perform(get(pathUrl + "/" + id))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertThat(result).contains("\"id\":"+id);
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(1);
    }

    @Test
    void getStatusAccessDeniedException() throws Exception {
        String id = "1";
        this.mockMvc.perform(get(pathUrl + "/" + id))
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }

    @Test
    @WithMockUser(roles = {"app-admin", "app-user"})
    void getStatusNotExistException() throws Exception {
        String id = "20000001";
        this.mockMvc.perform(get(pathUrl + "/" + id))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(NotFoundCustomRuntimeException.class));
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void getStatusesAsUserRole() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        String result = this.mockMvc.perform(get(pathUrl))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertThat(result).contains("\"id\":1");
        assertThat(result).contains("\"id\":2");
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(1);
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void getStatusesAsAdminRole() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        String result = this.mockMvc.perform(get(pathUrl))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertThat(result).contains("\"id\":1");
        assertThat(result).contains("\"id\":2");
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(1);
    }

    @Test
    void getStatusesAccessDeniedException() throws Exception {
        this.mockMvc.perform(get(pathUrl))
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void createStatusAsAdminRole() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        BoardDto.Request.CreateAsRelation boardDto = new BoardDto.Request.CreateAsRelation(1L);
        StatusDto.Request.Create dto = new StatusDto.Request.Create("name", boardDto);
        String requestCreate = objectMapper.writeValueAsString(dto);
        String response = this.mockMvc.perform(post(pathUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestCreate)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();
        assertThat(response).contains("\"name\":\"name\"");
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(3);
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void createStatusAsUserRoleAccessDeniedException() throws Exception {
        BoardDto.Request.CreateAsRelation boardDto = new BoardDto.Request.CreateAsRelation(1L);
        StatusDto.Request.Create dto = new StatusDto.Request.Create("name", boardDto);
        String requestCreate = objectMapper.writeValueAsString(dto);
        this.mockMvc.perform(post(pathUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestCreate)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void deleteStatusAsAdminRoleDataIntegrityViolationException() throws Exception {
        this.mockMvc.perform(delete(pathUrl + "/3"))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(DataIntegrityViolationException.class));
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void deleteStatusAsUserRoleAccessDeniedException() throws Exception {
        this.mockMvc.perform(delete(pathUrl + "/3"))
                .andExpect(status().isUnauthorized())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void deleteStatusAsAdminRoleNotFoundException() throws Exception {
        this.mockMvc.perform(delete( pathUrl + "/200000001"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void updateStatusAsAdminRole() throws Exception{
        hibernateQueryInterceptor.startQueryCount();
        BoardDto.Request.CreateAsRelation boardDto = new BoardDto.Request.CreateAsRelation(1L);
        StatusDto.Request.Create dto = new StatusDto.Request.Create("name", boardDto);
        String requestUpdate = objectMapper.writeValueAsString(dto);
        this.mockMvc.perform(put(pathUrl + "/3")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestUpdate)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(3);
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void updateStatusAsUserRoleAccessDeniedException() throws Exception{
        BoardDto.Request.CreateAsRelation boardDto = new BoardDto.Request.CreateAsRelation(1L);
        StatusDto.Request.Create dto = new StatusDto.Request.Create("name", boardDto);
        String requestUpdate = objectMapper.writeValueAsString(dto);
        this.mockMvc.perform(put(pathUrl + "/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestUpdate)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }
}
