package ru.tinkoff.board.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yannbriancon.interceptor.HibernateQueryInterceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.tinkoff.board.ApplicationIntegrationTest;
import ru.tinkoff.board.api.dto.NoteDto;
import ru.tinkoff.board.security.exception.NotFoundCustomRuntimeException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
public class NoteControllerIntegrationTest extends ApplicationIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private HibernateQueryInterceptor hibernateQueryInterceptor;

    private String pathUrl = "/notes";

    @AfterEach
    void tearDown() {
        cleanAndMigrate();
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void getNoteWithUserRole() throws Exception {
        String id = "1";
        hibernateQueryInterceptor.startQueryCount();
        String result = this.mockMvc.perform(get(pathUrl +"/" + id))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertThat(result).contains("\"id\":"+id);
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(2);
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void getNoteWithAdminRole() throws Exception {
        String id = "1";
        hibernateQueryInterceptor.startQueryCount();
        String result = this.mockMvc.perform(get(pathUrl + "/" + id))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertThat(result).contains("\"id\":"+id);
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(2);
    }

    @Test
    void getNoteAccessDeniedException() throws Exception {
        String id = "1";
        this.mockMvc.perform(get(pathUrl + "/" + id))
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }

    @Test
    @WithMockUser(roles = {"app-admin", "app-user"})
    void getNoteNotExistException() throws Exception {
        String id = "20000001";
        this.mockMvc.perform(get(pathUrl + "/" + id))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(NotFoundCustomRuntimeException.class));
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void getNotesAsUserRole() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        String result = this.mockMvc.perform(get(pathUrl))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertThat(result).contains("\"id\":1");
        assertThat(result).contains("\"id\":2");
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(9);
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void getNotesAsAdminRole() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        String result = this.mockMvc.perform(get(pathUrl))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertThat(result).contains("\"id\":1");
        assertThat(result).contains("\"id\":2");
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(9);
    }

    @Test
    void getNotesAccessDeniedException() throws Exception {
        this.mockMvc.perform(get(pathUrl))
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void createNoteAsAdminRole() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        NoteDto.Request.CreateAsRelation createAsRelation1 = new NoteDto.Request.CreateAsRelation(1L);
        NoteDto.Request.CreateAsRelation createAsRelation2 = new NoteDto.Request.CreateAsRelation(2L);
        NoteDto.Request.Create dto = new NoteDto.Request.Create("name", "name", "name", List.of(createAsRelation1), List.of(createAsRelation2));
        String requestCreate = objectMapper.writeValueAsString(dto);
        String response = this.mockMvc.perform(post(pathUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestCreate)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();
        assertThat(response).contains("\"name\":\"name\"");
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(6);
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void createNoteAsUserRoleAccessDeniedException() throws Exception {
        NoteDto.Request.CreateAsRelation createAsRelation1 = new NoteDto.Request.CreateAsRelation(1L);
        NoteDto.Request.CreateAsRelation createAsRelation2 = new NoteDto.Request.CreateAsRelation(2L);
        NoteDto.Request.Create dto = new NoteDto.Request.Create("name", "name", "name", List.of(createAsRelation1), List.of(createAsRelation2));
        String requestCreate = objectMapper.writeValueAsString(dto);
        this.mockMvc.perform(post(pathUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestCreate)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void deleteNoteAsAdminRole() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        this.mockMvc.perform(delete(pathUrl + "/3"))
                .andExpect(status().isOk());
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(2);
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void deleteNoteAsUserRoleAccessDeniedException() throws Exception {
        this.mockMvc.perform(delete(pathUrl + "/3"))
                .andExpect(status().isUnauthorized())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void deleteNoteAsAdminRoleNotFoundException() throws Exception {
        this.mockMvc.perform(delete( pathUrl + "/200000001"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = {"app-admin"})
    void updateNoteAsAdminRole() throws Exception{
        hibernateQueryInterceptor.startQueryCount();
        NoteDto.Request.CreateAsRelation createAsRelation1 = new NoteDto.Request.CreateAsRelation(1L);
        NoteDto.Request.CreateAsRelation createAsRelation2 = new NoteDto.Request.CreateAsRelation(2L);
        NoteDto.Request.Create dto = new NoteDto.Request.Create("name", "name", "name", List.of(createAsRelation1), List.of(createAsRelation2));
        String requestUpdate = objectMapper.writeValueAsString(dto);
        this.mockMvc.perform(put(pathUrl + "/3")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestUpdate)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(9);
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void updateStatusAsUserRoleAccessDeniedException() throws Exception{
        NoteDto.Request.CreateAsRelation createAsRelation1 = new NoteDto.Request.CreateAsRelation(1L);
        NoteDto.Request.CreateAsRelation createAsRelation2 = new NoteDto.Request.CreateAsRelation(2L);
        NoteDto.Request.Create dto = new NoteDto.Request.Create("name", "name", "name", List.of(createAsRelation1), List.of(createAsRelation2));
        String requestUpdate = objectMapper.writeValueAsString(dto);
        this.mockMvc.perform(put(pathUrl + "/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestUpdate)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AccessDeniedException.class));
    }

    @Test
    @WithMockUser(roles = {"app-user"})
    void findNoteByTagTest() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        this.mockMvc.perform(get(pathUrl + "/tag")
                        .param("tag", "note")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertThat(hibernateQueryInterceptor.getQueryCount()).isEqualTo(5);
    }
}
