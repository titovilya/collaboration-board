INSERT INTO fintech_boards
VALUES
    (nextval('board_sequence_gen'), now()::timestamp, 'hr', now()::timestamp, 'Board for hr purpose'),
    (nextval('board_sequence_gen'), now()::timestamp, 'dev', now()::timestamp, 'Board for developer purpose'),
    (nextval('board_sequence_gen'), now()::timestamp, 'hd', now()::timestamp, 'Board for helpdesk purpose');

INSERT INTO fintech_users
VALUES
    (nextval('user_sequence_gen'), 'Max Verstappen', 'verstappen@tinkoff.ru', 'ver'),
    (nextval('user_sequence_gen'), 'Sebastian Vettel', 'vettel@tinkoff.ru', 'vet'),
    (nextval('user_sequence_gen'), 'George Russel', 'russel@tinkoff.ru', 'rus');

INSERT INTO fintech_code_frames
VALUES
    (nextval('code_frame_sequence_gen'), now()::timestamp, 'feature1', 'feature', now()::timestamp, 'monkey_patch() for feature1', 1),
    (nextval('code_frame_sequence_gen'), now()::timestamp, 'feature2', 'feature', now()::timestamp, 'hotfix for feature2', 2),
    (nextval('code_frame_sequence_gen'), now()::timestamp, 'feature3', 'featurePlus', now()::timestamp, 'add dependency for feature3', 3),
    (nextval('code_frame_sequence_gen'), now()::timestamp, 'feature4', 'featurePlus', now()::timestamp, 'test for feature4', 3);

INSERT INTO fintech_notes
VALUES
    (nextval('note_sequence_gen'), now()::timestamp, 'note1', now()::timestamp, 'note', 'smth for 1'),
    (nextval('note_sequence_gen'), now()::timestamp, 'note2', now()::timestamp, 'note', 'smth for 2'),
    (nextval('note_sequence_gen'), now()::timestamp, 'note3', now()::timestamp, 'notePlus', 'smth for 3'),
    (nextval('note_sequence_gen'), now()::timestamp, 'note4', now()::timestamp, 'notePlus', 'smth for 4');

INSERT INTO fintech_referencing_notes
VALUES
    (1, 2),
    (1, 3),
    (1, 4),
    (3, 4),
    (3, 1);

INSERT INTO fintech_statuses
VALUES
    (nextval('status_sequence_gen'), 'TODO', 1),
    (nextval('status_sequence_gen'), 'IN_PROGRESS', 1),
    (nextval('status_sequence_gen'), 'DONE', 1),
    (nextval('status_sequence_gen'), 'TODO', 2),
    (nextval('status_sequence_gen'), 'DONE', 2),
    (nextval('status_sequence_gen'), 'TEST', 3);

INSERT INTO fintech_tasks
VALUES
    (nextval('task_sequence_gen'), now()::timestamp, 'task1', now()::timestamp, 'task1 descrption', 'task', 1, 1, 1),
    (nextval('task_sequence_gen'), now()::timestamp, 'task2', now()::timestamp, 'task2 descrption', 'taskPlus', 1, 2, 1),
    (nextval('task_sequence_gen'), now()::timestamp, 'task3', now()::timestamp, 'task3 descrption', 'task', 1, 3, 2),
    (nextval('task_sequence_gen'), now()::timestamp, 'task4', now()::timestamp, 'task4 descrption', 'taskPlus', 1, 3, 3),
    (nextval('task_sequence_gen'), now()::timestamp, 'task5', now()::timestamp, 'task5 descrption', 'task', 2, 4, 1),
    (nextval('task_sequence_gen'), now()::timestamp, 'task6', now()::timestamp, 'task6 descrption', 'taskPlus', 2, 5, 1),
    (nextval('task_sequence_gen'), now()::timestamp, 'task7', now()::timestamp, 'task7 descrption', 'task', 1, 4, 3),
    (nextval('task_sequence_gen'), now()::timestamp, 'task8', now()::timestamp, 'task8 descrption', 'taskPlus', 3, 6, 3),
    (nextval('task_sequence_gen'), now()::timestamp, 'task9', now()::timestamp, 'task9 descrption', 'task', 3, 6, 2);
