ALTER TABLE IF EXISTS fintech_referencing_notes
    DROP CONSTRAINT IF EXISTS fk_fintech_referencing_notes_referenced_note_id;
ALTER TABLE IF EXISTS fintech_referencing_notes
    DROP CONSTRAINT IF EXISTS fk_fintech_referencing_notes_referencing_note_id;
ALTER TABLE IF EXISTS fintech_statuses
    DROP CONSTRAINT IF EXISTS fk_statuses_board_id;
ALTER TABLE IF EXISTS fintech_tasks
    DROP CONSTRAINT IF EXISTS fk_tasks_status_id;
ALTER TABLE IF EXISTS fintech_tasks
    DROP CONSTRAINT IF EXISTS fk_tasks_user_id;
ALTER TABLE IF EXISTS fintech_tasks
    DROP CONSTRAINT IF EXISTS fk_tasks_board_id;
ALTER TABLE IF EXISTS fintech_code_frames
    DROP CONSTRAINT IF EXISTS fk_code_frames_user_id;

DROP SEQUENCE IF EXISTS board_sequence_gen;
DROP SEQUENCE IF EXISTS note_sequence_gen;
DROP SEQUENCE IF EXISTS status_sequence_gen;
DROP SEQUENCE IF EXISTS task_sequence_gen;
DROP SEQUENCE IF EXISTS user_sequence_gen;
DROP SEQUENCE IF EXISTS code_frame_sequence_gen;

DROP TABLE IF EXISTS fintech_boards CASCADE;
DROP TABLE IF EXISTS fintech_notes CASCADE;
DROP TABLE IF EXISTS fintech_statuses CASCADE;
DROP TABLE IF EXISTS fintech_tasks CASCADE;
DROP TABLE IF EXISTS fintech_users CASCADE;
DROP TABLE IF EXISTS fintech_code_frames CASCADE;

CREATE SEQUENCE board_sequence_gen START 1 INCREMENT 1;
CREATE SEQUENCE note_sequence_gen START 1 INCREMENT 1;
CREATE SEQUENCE status_sequence_gen START 1 INCREMENT 1;
CREATE SEQUENCE task_sequence_gen START 1 INCREMENT 1;
CREATE SEQUENCE user_sequence_gen START 1 INCREMENT 1;
CREATE SEQUENCE code_frame_sequence_gen START 1 INCREMENT 1;

CREATE TABLE fintech_boards (
                                id INT8 PRIMARY KEY,
                                created_on TIMESTAMP NOT NULL,
                                name VARCHAR(255) NOT NULL,
                                updated_on TIMESTAMP NOT NULL,
                                description VARCHAR(1023)
);

CREATE TABLE fintech_notes (
                               id INT8 PRIMARY KEY,
                               created_on TIMESTAMP NOT NULL,
                               name VARCHAR(255) NOT NULL,
                               updated_on TIMESTAMP NOT NULL,
                               tag VARCHAR(255),
                               text VARCHAR(1023)
);

CREATE TABLE fintech_statuses (
                                  id INT8 PRIMARY KEY,
                                  name VARCHAR(255) NOT NULL,
                                  board_id INT8 NOT NULL
);

CREATE TABLE fintech_tasks (
                               id INT8 PRIMARY KEY,
                               created_on TIMESTAMP NOT NULL,
                               name VARCHAR(255) NOT NULL,
                               updated_on TIMESTAMP NOT NULL,
                               description VARCHAR(1023),
                               tag VARCHAR(255),
                               board_id int8 NOT NULL,
                               status_id INT8,
                               user_id INT8
);

CREATE TABLE fintech_users (
                               id INT8 PRIMARY KEY,
                               name VARCHAR(255),
                               email VARCHAR(255),
                               username VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE fintech_referencing_notes (
                                           referenced_note_id INT8 NOT NULL,
                                           referencing_note_id INT8 NOT NULL
);

CREATE TABLE fintech_code_frames (
                                     id INT8 PRIMARY KEY,
                                     created_on TIMESTAMP NOT NULL,
                                     name VARCHAR(255) NOT NULL,
                                     tag VARCHAR(255),
                                     updated_on TIMESTAMP NOT NULL,
                                     text VARCHAR(1023),
                                     user_id INT8
);

ALTER TABLE fintech_statuses
    ADD CONSTRAINT fk_statuses_board_id
        FOREIGN KEY (board_id) REFERENCES fintech_boards;

ALTER TABLE fintech_tasks
    ADD CONSTRAINT fk_tasks_status_id
        FOREIGN KEY (status_id) REFERENCES fintech_statuses;

ALTER TABLE fintech_tasks
    ADD CONSTRAINT fk_tasks_user_id
        FOREIGN KEY (user_id) REFERENCES fintech_users;

ALTER TABLE fintech_tasks
    ADD CONSTRAINT fk_tasks_board_id
        FOREIGN KEY (board_id) REFERENCES fintech_boards;

ALTER TABLE fintech_referencing_notes
    ADD CONSTRAINT fk_fintech_referencing_notes_referenced_note_id
        FOREIGN KEY (referenced_note_id) REFERENCES fintech_notes;

ALTER TABLE fintech_referencing_notes
    ADD CONSTRAINT fk_fintech_referencing_notes_referencing_note_id
        FOREIGN KEY (referencing_note_id) REFERENCES fintech_notes;

ALTER TABLE fintech_code_frames
    ADD CONSTRAINT fk_code_frames_user_id
        FOREIGN KEY (user_id) references fintech_users;

CREATE INDEX note_tag_index ON fintech_notes (tag);
CREATE INDEX status_board_id_index ON fintech_statuses (board_id);
CREATE INDEX task_tag_index ON fintech_tasks (tag);
CREATE INDEX task_user_id_index ON fintech_tasks (user_id);
CREATE INDEX user_username_index ON fintech_users (username);
